def sorry(string):
	"""
    Print the string. 
    
    :param string: String to be printed
    :type string: string
	"""
	print(string)

sorry("Sorry")
