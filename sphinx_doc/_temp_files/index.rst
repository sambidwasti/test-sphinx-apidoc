.. test-sphinx-apidoc documentation master file, created by
   sphinx-quickstart on Tue Jun 22 13:37:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test-sphinx-apidoc's documentation!
==============================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   
   readme.md
   modules
   nextlevel
   notebook


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
