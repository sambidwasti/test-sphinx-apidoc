# [TEST SPHINX APIDOC](https://sambidwasti.gitlab.io/test-sphinx-apidoc)

## This is working. ##

Now need to see if i add a file, will this handle the documentation automatically or do i need to run sphinx and update that too?
One hand i might not need it as i am just running conf.py file. However, the index might need update. Or can i update it... ??

## Warning ##
* The added updated scripts are not documented. 
* If there is an issue with the documentation/scripts/etc. The scripts will not be added in the documentation. 
    * If the scripts were ran locally, we would see errors and warning but somehow these were masked. 
    * So if there is an error, double check the document.

## Issues and tried resolutions ##
* Pandoc Gitlab issue
    * Found few work arounds. They are documented in the one note. 
    * Issue 
        * Pandoc installs in local repo via pip install pandoc. 
        * This doesnt work in gitlabci, the command executes but pandoc is not installed.
    * solutions: 2 found.
        * Use a docker file with proper image.
            * [Solution found here](https://github.com/spatialaudio/nbsphinx/issues/583)
        * Use apt-get to install the pandoc.
            * [solution found from here](https://github.com/spatialaudio/nbsphinx/issues/583)
